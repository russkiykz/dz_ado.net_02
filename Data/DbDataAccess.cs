﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Data
{
    public abstract class DbDataAccess<T> : IDisposable
    {
        protected readonly SqlConnection connection;
        public DbDataAccess()
        {
            connection = new SqlConnection();
            connection.ConnectionString = "Server=DESKTOP-0V33MK0\\MSSQLSERVER01; Database = Library; Trusted_Connection=true;";
            connection.Open();
        }
        public void Dispose()
        {
            connection.Close();
        }
        public abstract void Update(T entity);
        public abstract ICollection<T> Select();
    }
}
