﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Data
{
    public class SheetDataAccess : DbDataAccess<Sheet>
    {
        public override ICollection<Sheet> Select()
        {
            string selectSqlScript = @"select * from Sheet
                                        join Users on Users.Id=Sheet.UserId
                                        join Books on Books.Id=Sheet.BookId
                                        join Authors on Authors.Id=Sheet.AuthorId";
            //string selectSqlScript = "select * from Users";
            SqlCommand command = new SqlCommand(selectSqlScript, connection);
            var dataReader = command.ExecuteReader();
            List<Sheet> sheets = new List<Sheet>();
            while (dataReader.Read())
            {
                sheets.Add(new Sheet
                {
                    User = new User
                    {
                        Id = int.Parse(dataReader["UserId"].ToString()),
                        UserName = dataReader["UserName"].ToString()
                    },
                    Debt = bool.Parse(dataReader["Debt"].ToString()),
                    Book = new Book
                    {
                        Id= int.Parse(dataReader["BookId"].ToString()),
                        BookName = dataReader["BookName"].ToString()
                    },
                    Author = new Author
                    {
                        Id=int.Parse(dataReader["AuthorId"].ToString()),
                        AuthorName = dataReader["AuthorName"].ToString()
                    }
                });
            }

            dataReader.Close();
            return sheets;
        }

        public override void Update(Sheet entity)
        {
            throw new NotImplementedException();
        }
    }
}
