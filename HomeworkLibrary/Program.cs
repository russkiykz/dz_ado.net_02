﻿using Data;
using Models;
using System;
using System.Collections.Generic;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            var sheetDataAccess = new SheetDataAccess();
            var listLibrary = sheetDataAccess.Select();

            Console.WriteLine("1) Выведите список должников.\nСписок: ");
            foreach(var element in listLibrary)
            {
                if (element.Debt == true)
                {
                    Console.WriteLine($"#{element.User.Id} - {element.User.UserName}");
                }
            }

            Console.WriteLine("\n2) Выведите список авторов книги №3 (по порядку из таблицы ‘Book’).\nСписок: ");
            foreach (var element in listLibrary)
            {
                if (element.Book.Id == 3)
                {
                    Console.WriteLine($"{element.Author.AuthorName}");
                }
            }

            Console.WriteLine("\n4) Вывести список книг, которые на руках у пользователя №2.\nСписок: ");
            foreach (var element in listLibrary)
            {
                if (element.User.Id == 2)
                {
                    Console.WriteLine($"{element.Book.BookName}");
                }
            }

        }
    }
}
