﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Sheet
    {
        public User User { get; set; }
        public bool Debt { get; set; }
        public Book Book { get; set; }
        public Author Author { get; set; }
    }
}
