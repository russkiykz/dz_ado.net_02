﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Author
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
    }
}
